package com.example.luis.donaraqp;

import android.app.Activity;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.luis.donaraqp.R;
import com.example.luis.donaraqp.Word;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Enrique on 12/24/2017.
 */


public class WordAdapter extends ArrayAdapter<Word> {

    private int mColorResourceId;


    public WordAdapter(Activity context, ArrayList<Word> words, int ColorResourceId){

        super(context,0,words);
        mColorResourceId=ColorResourceId;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItemView= convertView;

        if(listItemView==null){
            listItemView= LayoutInflater.from(getContext()).inflate(
                    R.layout.item_list,parent,false);
        }

        Word currentWord =  getItem(position);

        TextView tituloView= (TextView) listItemView.findViewById(R.id.titulo_view);


        TextView defaultTextView = (TextView) listItemView.findViewById(R.id.texto_view);
        defaultTextView.setText(currentWord.getTextoEvento());

        TextView lugarTextView = (TextView) listItemView.findViewById(R.id.lugar_view);


        ImageView Image= (ImageView) listItemView.findViewById(R.id.sangre_image_view);


        if(currentWord.hasImage()==true){
            Image.setBackgroundResource(currentWord.getImageResource());}

        else {
            Image.setVisibility(View.GONE);
        }



        if(currentWord.hasText()==true){
            lugarTextView.setText(currentWord.getLugarEvento());
        }
        else{
            lugarTextView.setVisibility(View.GONE);
        }


        if(currentWord.hasTittle()==true){
           tituloView.setText(currentWord.getTituloEvento());
        }
        else{
            lugarTextView.setVisibility(View.GONE);
        }


        //Colores de fondo

        View textContainer = listItemView.findViewById(R.id.textContainer);
        int color = ContextCompat.getColor(getContext(),mColorResourceId);
        textContainer.setBackgroundColor(color);

        return listItemView;

    }
}