package com.example.luis.donaraqp;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Response;


public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "mysharedpref12";
    private static final String KEY_NAME = "username";
    private static final String KEY_USER_EMAIL = "useremail";
    private static final String KEY_USER_ID = "userid";
    private static final String KEY_SURNAME= "surname";
    private static final String KEY_EDAD = "edad";
    private static final String KEY_GENERO= "genero";
    private static final String KEY_PERMISO = "permiso";
    private static final String KEY_SANGRE= "sangre";


    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public boolean userLogin(int id,String nombre,String apellido,String edad,String genero, String email,int permiso,String sangre){

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_USER_ID,Integer.toString(id));
        editor.putString(KEY_PERMISO,Integer.toString(permiso));
        editor.putString(KEY_USER_EMAIL,email);
        editor.putString(KEY_NAME,nombre);
        editor.putString(KEY_SURNAME,apellido);
        editor.putString(KEY_EDAD,edad);
        editor.putString(KEY_GENERO,genero);
        editor.putString(KEY_SANGRE,sangre);

        editor.apply();

        return true;

    }

    public boolean setDatos(String nombre,String apellido){

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(KEY_USER_ID,this.getId());
        editor.putString(KEY_PERMISO,this.getPermiso());
        editor.putString(KEY_USER_EMAIL,this.getEmail());
        editor.putString(KEY_NAME,nombre);
        editor.putString(KEY_SURNAME,apellido);
        editor.putString(KEY_EDAD,this.getEdad());
        editor.putString(KEY_GENERO,this.getGenero());
        editor.putString(KEY_SANGRE,this.getTipoSangre());

        editor.apply();

        return true;

    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        if(sharedPreferences.getString(KEY_USER_EMAIL,null) != null){
            return true;
        }
        return false;
    }

    public boolean logout(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }

    public String getNombres(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NAME,null);
    }

    public String getApellidos(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_SURNAME,null);
    }

    public String getEdad(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_EDAD,null);
    }
    public String getGenero(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_GENERO,null);
    }
    public String getEmail(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_EMAIL,null);
    }

    public String getTipoSangre(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_SANGRE,null);
    }

    public String getPermiso(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PERMISO,null);
    }

    public String getId(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USER_ID,null);
    }


}
