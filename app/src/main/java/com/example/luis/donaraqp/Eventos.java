package com.example.luis.donaraqp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class Eventos extends Fragment {

    ArrayList<Word> words;

    public Eventos() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ArrayList<Word> saved=new ArrayList<Word>();

        final View rootView = inflater.inflate(R.layout.fragment_eventos,container,false);

        //swipe
        final SwipeRefreshLayout swipeRefreshLayout;
        swipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.swiperefreshlayout);

        words= new ArrayList<Word>();

        saved=loadData(saved);

        words.addAll(saved);


        final WordAdapter adapter= new WordAdapter(getActivity(), words,  R.color.colorList);



        final ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                refresh(adapter,listView,swipeRefreshLayout);
            }
        });

        //clearData();
        if(isEmpty(saved)){
            refresh(adapter,listView,swipeRefreshLayout);
        }
        //saveData(words);


        return rootView;

    }

    private int idImagen(String a){
        String tipo = a.substring(0,a.length()-1);
        if(tipo.equals("A")) return 1;
        else if(tipo.equals("B")) return 2;
        else if(tipo.equals("AB")) return 3;
        else if(tipo.equals("O")) return 4;
        else return 5;
    }

    private void refresh(final WordAdapter adapter, final ListView listview, final SwipeRefreshLayout swipeRefreshLayout){

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_EVENTOS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if(!obj.getBoolean("error")){
                                clearData();
                                words.clear();
                                JSONObject evento;
                                for (int i=1;i<=obj.getInt("Cantidad");i++) {
                                    evento = new JSONObject(obj.getString(Integer.toString(i)));
                                    words.add(new Word(evento.getString("Titulo"), evento.getString("Mensaje"),evento.getString("nombreCentro"),
                                            pickImage(idImagen(evento.getString("TipoSangre")))));
                                }
                                listview.setAdapter(adapter);
                                listview.invalidateViews();
                                swipeRefreshLayout.setRefreshing(false);
                                Collections.reverse(words); // invierte el orden de words
                                saveData(words);
                            }else{
                                swipeRefreshLayout.setRefreshing(false);
                                Toast.makeText(
                                        getContext(),
                                        obj.getString("message"),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getContext(),"No hay internet",Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return new HashMap<>();
            }
        };

        RequestHandler.getInstance(getContext()).addToRequestQueue(stringRequest);

    }

    private int pickImage(int num)
    {

        int Image=0;

        switch (num){

            case 1:
                Image =  R.drawable.blood_a;
                return Image;
            case 2:
                Image =  R.drawable.blood_b;
                return Image;

            case 3:
                Image = R.drawable.blood_ab;
                return Image;

            case 4:
                Image =  R.drawable.blood_o;
                return Image;

            case 5:
                Image =  R.drawable.donation;
                return Image;
        }
        return Image;
    }

    private void saveData(ArrayList<Word> words){
    SharedPreferences sharedPreferences = getActivity().getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    Gson gson = new Gson();
    String json = gson.toJson(words);
    editor.putString("Event list",json);
    editor.apply();
    }

    private ArrayList<Word> loadData(ArrayList<Word> words) {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Event list",null);
        Type type = new TypeToken<ArrayList<Word>>() {}.getType();
        words = gson.fromJson(json,type);

        if(words==null) return new ArrayList<Word>();

        return words;

    }

    private void clearData(){

        SharedPreferences preferences = getActivity().getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();

    }

    private boolean isEmpty(ArrayList<Word> words){

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("shared preferences", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("Event list",null);
        Type type = new TypeToken<ArrayList<Word>>() {}.getType();
        words = gson.fromJson(json,type);

        if(words==null) return true;

        return false;

    }


}
