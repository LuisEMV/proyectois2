package com.example.luis.donaraqp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class info extends Fragment {

    public info() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_info,container,false);
        // Create a list of words
        final ArrayList<Word> words = new ArrayList<Word>();



        words.add(new Word(getResources().getString(R.string.txtHappy),R.drawable.happy));
        words.add(new Word(getResources().getString(R.string.txtAge),R.drawable.mayor18));
        words.add(new Word(getResources().getString(R.string.txtDNI),R.drawable.dni));
        words.add(new Word(getResources().getString(R.string.txtWeight),R.drawable.scale));
        words.add(new Word(getResources().getString(R.string.txtFood),R.drawable.apple));
        words.add(new Word(getResources().getString(R.string.txtMedicine),R.drawable.drugs));
        words.add(new Word(getResources().getString(R.string.txtTattoo),R.drawable.tattoo));
        words.add(new Word(getResources().getString(R.string.txtAlcohol),R.drawable.beer));
        words.add(new Word(getResources().getString(R.string.txtHepatitis),R.drawable.hepatitis));
        words.add(new Word(getResources().getString(R.string.txtCirugiaMenor),R.drawable.scalpel));
        words.add(new Word(getResources().getString(R.string.txtCirugiaMayor),R.drawable.operation));
        words.add(new Word(getResources().getString(R.string.txtVirus),R.drawable.virus));
        words.add(new Word(getResources().getString(R.string.txtCorazon),R.drawable.cardiogram));
        words.add(new Word(getResources().getString(R.string.txtEmbarazada),R.drawable.pregnant));
        words.add(new Word(getResources().getString(R.string.txtPareja),R.drawable.genders));



        final WordAdapter adapter = new WordAdapter(getActivity(), words,  R.color.colorList);
        final ListView listView = (ListView) rootView.findViewById(R.id.list_item);
        listView.setAdapter(adapter);



        //Hyperlink

        TextView link = (TextView) rootView.findViewById(R.id.preguntas);
        link.setMovementMethod(LinkMovementMethod.getInstance());



        return rootView;
    }
}
