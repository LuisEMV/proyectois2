package com.example.luis.donaraqp;

/**
 * Created by luis on 11/20/17.
 */

public class Constants {

    //private static final String ROOT_URL = "http://badsoftweb.com/yo_dono_sangre_AQP/v1/";

    private static final String ROOT_URL = "http://192.168.43.178/donaAQPWeb/v1/";

    public static final String URL_REGISTER = ROOT_URL + "registerUser.php";
    public static final String URL_LOGIN = ROOT_URL + "userLogin.php";
    public static final String URL_EVENTOS= ROOT_URL + "obtenerEventos.php";
    public static final String URL_CD= ROOT_URL + "obtenerCentroDonacion.php";
    public static final String URL_CREAR_EVENTO= ROOT_URL + "crearEvento.php";
    public static final String URL_EDITAR= ROOT_URL + "editarPersona.php";
}
