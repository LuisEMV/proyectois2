package com.example.luis.donaraqp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class editarPerfil extends AppCompatActivity implements View.OnClickListener {

    EditText nombres,apellidos,password;
    Button botonEditar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        nombres = (EditText) findViewById(R.id.editTextNombres);
        apellidos = (EditText) findViewById(R.id.editTextApellidos);
        password = (EditText) findViewById(R.id.editTextContrasena);
        botonEditar = (Button) findViewById(R.id.buttonEditarPerfil);

        progressDialog = new ProgressDialog(this);

        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return ;
        }

        nombres.setText(SharedPrefManager.getInstance(this).getNombres());
        apellidos.setText(SharedPrefManager.getInstance(this).getApellidos());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        botonEditar.setOnClickListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MiPerfil.class);
        finish();
        startActivityForResult(myIntent, 0);
        return true;
    }

    public void editarPerfil(){

        final String v0 = SharedPrefManager.getInstance(this).getId();
        final String v1 = nombres.getText().toString().trim();
        final String v2 = apellidos.getText().toString().trim();
        final String v3 = password.getText().toString().trim();

        progressDialog.setMessage("Editando Datos...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_EDITAR,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                            if(!jsonObject.getBoolean("error")){
                                SharedPrefManager.getInstance(editarPerfil.this).setDatos(v1,v2);
                                finish();
                                startActivity(new Intent(getApplicationContext(),MiPerfil.class));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(),"No hay internet",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id",v0);
                params.put("nombres",v1);
                params.put("apellidos",v2);
                params.put("pass",v3);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if(v == botonEditar){
            editarPerfil();
        }
    }
}
