package com.example.luis.donaraqp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class crearEvento extends AppCompatActivity implements View.OnClickListener {


    Calendar myCalendar;
    EditText edittext,titulo,mensaje;
    Button crear;
    private Spinner editTipoSangre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_evento);

        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return ;
        }

        myCalendar = Calendar.getInstance();

        edittext = (EditText) findViewById(R.id.editTextFecha);
        titulo = (EditText) findViewById(R.id.editTextTitulo);
        mensaje = (EditText) findViewById(R.id.editTextMensaje);
        crear = (Button) findViewById(R.id.buttonAgregarEvento);
        editTipoSangre = (Spinner) findViewById(R.id.editTipoSangre);

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(crearEvento.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        crear.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittext.setText(sdf.format(myCalendar.getTime()));
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), TabbedMain.class);
        finish();
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void crearEvento(){
        final String v1 = SharedPrefManager.getInstance(this).getId();
        final String v2 = titulo.getText().toString();
        final String v3 = mensaje.getText().toString();
        final String v4 = edittext.getText().toString();
        final String v5 = editTipoSangre.getSelectedItem().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_CREAR_EVENTO,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                            if(!jsonObject.getBoolean("error")){
                                finish();
                                startActivity(new Intent(getApplicationContext(),TabbedMain.class));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("idRepresentante",v1);
                params.put("titulo",v2);
                params.put("mensaje",v3);
                params.put("fecha",v4);
                params.put("tipoSangre",v5);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if(v == crear)
            crearEvento();
    }
}
