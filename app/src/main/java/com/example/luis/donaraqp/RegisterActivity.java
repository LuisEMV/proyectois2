package com.example.luis.donaraqp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextNombres,editTextContrasena,editTextCorreo,editTextApellidos,editTextEdad;
    private Button buttonRegister;
    private Spinner editTipoSangre,editGenero;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if(SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(getApplicationContext(), TabbedMain.class));
            return ;
        }

        editTextCorreo= (EditText) findViewById(R.id.editTextEmail);
        editTextNombres= (EditText) findViewById(R.id.editTextNombres);
        editTextContrasena = (EditText) findViewById(R.id.editTextContrasena);
        editTextApellidos = (EditText) findViewById(R.id.editTextApellidos);
        editTextEdad = (EditText) findViewById(R.id.editTextEdad);
        editTipoSangre = (Spinner) findViewById(R.id.editTipoSangre);
        editGenero = (Spinner) findViewById(R.id.editGenero);

        buttonRegister = (Button) findViewById(R.id.buttonRegister);
        progressDialog = new ProgressDialog(this);

        buttonRegister.setOnClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), LoginActivity.class);
        finish();
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void registerUser(){


        final String email = editTextCorreo.getText().toString().trim();
        final String nombres = editTextNombres.getText().toString().trim();
        final String apellidos = editTextApellidos.getText().toString().trim();
        final String edad = editTextEdad.getText().toString().trim();
        final String password = editTextContrasena.getText().toString().trim();
        final String genero = editGenero.getSelectedItem().toString();
        final String tipoSangre = editTipoSangre.getSelectedItem().toString();

        progressDialog.setMessage("Registering user...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Constants.URL_REGISTER,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response){
                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                            if(!jsonObject.getBoolean("error")){
                                finish();
                                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("nombres",nombres);
                params.put("password",password);
                params.put("email",email);
                params.put("apellidos",apellidos);
                params.put("genero",genero);
                params.put("edad",edad);
                params.put("tipoSangre",tipoSangre);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if(v == buttonRegister)
            registerUser();
    }
}
