package com.example.luis.donaraqp;

/**
 * Created by Enrique on 12/24/2017.
 */

public class Word {

    private String mTituloEvento="";
    private String mTextoEvento;
    private String mLugarEvento="";
    private int mImageResource = -1;


    public Word(String TituloEvento,String TextoEvento, String LugarEvento, int ImageResource) {

        mTituloEvento = TituloEvento;
        mTextoEvento = TextoEvento;
        mLugarEvento = LugarEvento;
        mImageResource = ImageResource;

    }


    public Word(String TextoEvento,int ImageResource) {


        mTextoEvento = TextoEvento;
        mImageResource = ImageResource;

    }



    public String getTituloEvento() {
        return mTituloEvento;
    }

    public String getTextoEvento() {
        return mTextoEvento;
    }

    public String getLugarEvento() {return mLugarEvento;}

    public int getImageResource() {
        return mImageResource;
    }

    public boolean hasImage() {
        if (mImageResource == -1) return false;

        else return true;
    }

    public boolean hasText(){
        if(mLugarEvento.equals("")) return false;

        else return true;

    }

    public boolean hasTittle(){
        if(mTituloEvento.equals("")) return false;

        else return true;

    }



}
