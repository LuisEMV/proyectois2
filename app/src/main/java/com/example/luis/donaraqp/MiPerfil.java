package com.example.luis.donaraqp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MiPerfil extends AppCompatActivity implements View.OnClickListener {

    private TextView nombres,apellidos,edad,genero,correo,tipoSangre;
    private Button botonEditar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_perfil);

        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return ;
        }

        nombres = (TextView) findViewById(R.id.nombre);
        apellidos = (TextView) findViewById(R.id.apellidos);
        edad = (TextView) findViewById(R.id.edad);
        genero = (TextView) findViewById(R.id.genero);
        correo = (TextView) findViewById(R.id.correo);
        tipoSangre = (TextView) findViewById(R.id.sangre);
        botonEditar = (Button) findViewById(R.id.botonEditar);

        nombres.setText(SharedPrefManager.getInstance(this).getNombres());
        apellidos.setText(SharedPrefManager.getInstance(this).getApellidos());
        edad.setText(SharedPrefManager.getInstance(this).getEdad());
        genero.setText(SharedPrefManager.getInstance(this).getGenero());
        correo.setText(SharedPrefManager.getInstance(this).getEmail());
        tipoSangre.setText(SharedPrefManager.getInstance(this).getTipoSangre());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        botonEditar.setOnClickListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), TabbedMain.class);
        finish();
        startActivityForResult(myIntent, 0);
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v == botonEditar){
            finish();
            startActivity(new Intent(getApplicationContext(),editarPerfil.class));
        }
    }
}
